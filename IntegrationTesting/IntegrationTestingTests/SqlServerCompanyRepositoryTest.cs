using System;
using System.Data.SqlClient;
using System.Linq;
using CompanyManager.Core;
using CompanyManager.SqlServer;
using Dapper;

namespace IntegrationTestingTests; 
using System.Threading.Tasks;
using Xunit;

[Collection("SqlServer collection")]
public class SqlServerCompanyRepositoryTest {
    private readonly SqlServerFixture fixture;

    public SqlServerCompanyRepositoryTest(SqlServerFixture fixture) {
        this.fixture = fixture;
        connectionString = SqlServerConnectionHelper.ConnectionString;
    }
    private string connectionString;

    [Fact]
    public async Task FindAll() {
        var config = new SqlServerConfiguration(connectionString);
        CompanyRepository repo = new SqlServerCompanyRepository(config);
        // Creare la tabella: probabilmente altrove TODO
        // Inserire i dati nel DB
        await InsertCompany("01234567890", "First company");
        await InsertCompany("12345678901", "Second company");
        await InsertCompany("23456789012", "Third company");
        var result = await repo.FindAll();
        // Assertion
        Assert.Equal(3, result.Count());
    }
    
    [Fact]
    public async Task ShouldReadVatCodeProperly() {
        var config = new SqlServerConfiguration(connectionString);
        CompanyRepository repo = new SqlServerCompanyRepository(config);
        const string vatCode = "01234567890";
        await InsertCompany(vatCode, "First company");
        var result = (await repo.FindAll()).First();
        Assert.Equal(vatCode, result.VatCode);
    }

    private async Task InsertCompany(string vat, string businessName) {
        await using var conn = new SqlConnection(connectionString);
        await conn.ExecuteAsync("insert into Company (VatCode, BusinessName, CityId) values (@vat, @businessName, @cityId)", new { vat, businessName, cityId = SqlServerFixture.Brescia });
    }
}

public static class SqlServerConnectionHelper {
    public static string? ConnectionString => Environment.GetEnvironmentVariable("SQL_SERVER_TEST_CONNECTION_STRING") 
                            ?? "server=localhost;database=tempdb;user id=sa;password=SysAdminP4zz";
}
