using System;
using System.Data.SqlClient;
using Dapper;
using Xunit;

namespace IntegrationTestingTests;

[CollectionDefinition("SqlServer collection")]
public class SqlServerCollectionFixture : ICollectionFixture<SqlServerFixture> {
}

public class SqlServerFixture : IDisposable {
    public SqlServerFixture() {
        // Nella realtà uso le migrazioni
        using var conn = new SqlConnection(SqlServerConnectionHelper.ConnectionString);
        conn.Execute(@"
drop table if exists Company;
drop table if exists City;

create table City (
    Id INT identity(1,1),
    Name varchar(128),
    primary key(Id)
);

insert into City (Name) values ('Brescia');

create table Company (
    VatCode char(11),
    BusinessName varchar(200),
    CityId int not null,
    primary key (VatCode),
    foreign key (CityId) references City(Id)
);
", new { BresciaId = Brescia });
    }

    public const int Brescia = 19;

    public void Dispose() {
    }
}
