using System;
using System.Threading;
using CompanyManager.Core;
using Xunit;

namespace IntegrationTestingTests; 

public class CounterTest {
    [Fact]
    public void InitialCounterValueIsZero() {
        var counter = new Counter();
        Assert.Equal(0, counter.Value);
    }

    [Fact]
    public void CanProvideInitialValue() {
        const int initialValue = 19;
        var counter = new Counter(initialValue);
        Assert.Equal(initialValue, counter.Value);
    }

    [Fact]
    public void CanIncrement() {
        const int initialValue = 19;
        var counter = new Counter(initialValue);
        const int count = 10;
        for (int i = 0; i < count; i++) {
            counter.Increment();
        }
        Assert.Equal(initialValue + count, counter.Value);
    }

    [Fact]
    public void IncrementShouldBeThreadSafe() {
        
        var counter = new Counter();
        var startBarrier = new Barrier(threadCount);
        var endBarrier = new Barrier(threadCount + 1);
        for (int i = 0; i < threadCount; i++) {
            var t = new Thread(() => {
                startBarrier.SignalAndWait();
                for (int j = 0; j < incrementsPerThread; j++) {
                    counter.Increment();
                }
                endBarrier.SignalAndWait();
            });
            t.Start();
        }
        endBarrier.SignalAndWait();
        Assert.Equal(threadCount * incrementsPerThread, counter.Value);
    }
    
    const int threadCount = 10;
    const int incrementsPerThread = 10;

    
    [Fact]
    public void IncrementShouldBeThreadSafe_Reloaded() {
        var executor = new ConcurrentExecutor {
            RepetitionsCount = incrementsPerThread,
            ThreadCount = threadCount,
            RandomDelayBetweenExecutions = true
        };
        var counter = new Counter();
        executor.Execute(counter.Increment);
        Assert.Equal(executor.RepetitionsCount * executor.ThreadCount, counter.Value);
    }
}

public class ConcurrentExecutor {
    public int ThreadCount { get; set; } = 50;
    public int RepetitionsCount { get; set; } = 200;
    public bool RandomDelayBetweenExecutions { get; set; } = true;

    private readonly Random random = new Random();

    public void Execute(Action action) {
        Barrier startBarrier = new Barrier(ThreadCount);
        Barrier endBarrier = new Barrier(ThreadCount + 1);
        for (int i = 0; i < ThreadCount; i++) {
            new Thread(() => {
                startBarrier.SignalAndWait();
                for (int j = 0; j < RepetitionsCount; j++) {
                    if (RandomDelayBetweenExecutions) {
                        Thread.Sleep(random.Next(10));
                    }
                    action();
                }
                endBarrier.SignalAndWait();
            }).Start();
        }
        endBarrier.SignalAndWait();
    }
}