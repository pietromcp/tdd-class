using System.Data.SqlClient;
using CompanyManager.Core;
using Dapper;

namespace CompanyManager.SqlServer;

public class SqlServerCompanyRepository : CompanyRepository {
    private readonly SqlServerConfiguration config;

    public SqlServerCompanyRepository(SqlServerConfiguration config) {
        this.config = config;
    }

    public async Task<IEnumerable<Company>> FindAll() {
        await using var conn = new SqlConnection(config.ConnectionString);
        return await conn.QueryAsync<Company>("select VatCode, BusinessName from Company");
    }
}

public record SqlServerConfiguration(string ConnectionString);