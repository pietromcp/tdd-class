namespace CompanyManager.Core;

public class Counter {
    private readonly object syncRoot = new object();
    
    public Counter() : this(0) {
    }

    public Counter(int initialValue) {
        Value = initialValue;
    }

    public int Value { get; private set; }

    public void Increment() {
        // lock (syncRoot) {
        //     Value++;
        // }
        Value++;
    }
}