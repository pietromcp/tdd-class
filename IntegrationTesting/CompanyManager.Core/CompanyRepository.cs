namespace CompanyManager.Core;

public interface CompanyRepository {
    Task<IEnumerable<Company>> FindAll();
}

public class Company {
    public string VatCode { get; set; }
}