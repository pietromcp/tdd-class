using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using WeatherForecastWeb;
using Xunit;
using Xunit.Abstractions;

namespace WeatherForecastE2ETest;

public class VersionApiE2ETest : IDisposable {
    private const string VersionNumber = "1.2.3";
    private readonly TestServer server;
    private readonly HttpClient client;
    public VersionApiE2ETest(ITestOutputHelper testOutputHelper) {
        server = new TestServer(new WebHostBuilder().UseStartup<Startup>()
            .ConfigureTestServices(services => {
                services.AddSingleton(new AppVersionInfo(Version: VersionNumber));
            })
        );
        client = server.CreateClient();
    }

    public void Dispose() {
        client?.Dispose();
        server?.Dispose();
    }

    [Fact]
    public async Task CanGetAppVersionThroughGetApi() {
        var result = await client.GetAsync("/api/meta/version");
        Assert.Equal(HttpStatusCode.OK, result.StatusCode);
    }
    
    [Fact]
    public async Task VersionNumberOk() {
        var result = await client.GetAsync("/api/meta/version");
        var contentAsText = await result.Content.ReadAsStringAsync();
        var content = JsonConvert.DeserializeObject<VersionInfoDto>(contentAsText);
        var enumerable = VersionNumber;
        Assert.Equal(enumerable, content.Version);
    }
}

internal class VersionInfoDto {
    public string Version { get; set; }
}
