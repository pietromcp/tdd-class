namespace WeatherForecastWeb; 

public record AppVersionInfo(string Version);