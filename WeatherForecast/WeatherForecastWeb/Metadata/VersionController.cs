using Microsoft.AspNetCore.Mvc;

namespace WeatherForecastWeb.Metadata; 

[ApiController]
[Route("/api/meta/version")]
public class VersionController : ControllerBase {
    private readonly AppVersionInfo versionInfo;

    public VersionController(AppVersionInfo versionInfo) {
        this.versionInfo = versionInfo;
    }

    [HttpGet]
    public IActionResult Get() {
        return Ok(new { Version = versionInfo.Version });
    }
}
