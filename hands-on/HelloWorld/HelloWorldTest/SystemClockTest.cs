using System;
using HelloWorld;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace HelloWorldTest; 

public class SystemClockTest {
    [Fact]
    public void Before() {
        var clock = new SystemClock();
        var before = DateTimeOffset.Now;
        var now = clock.Now;
        Assert.That(now, Is.GreaterThanOrEqualTo(before));
    }
    
    [Fact]
    public void After() {
        var clock = new SystemClock();
        var now = clock.Now;
        var after = DateTimeOffset.Now;
        Assert.That(now, Is.LessThanOrEqualTo(after));
    }
}