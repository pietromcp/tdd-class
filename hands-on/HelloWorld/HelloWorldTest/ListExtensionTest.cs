using System;
using System.Collections.Generic;
using System.Linq;
using HelloWorld;
using NHamcrest;
using Xunit;

namespace HelloWorldTest; 

public class ListExtensionTest {
    [Fact]
    public void RandomShouldReturnElementFromTheList() {
        var list = new[] { "A", "B", "C", "D" };
        for (int i = 0; i < 100; i++) {
            var selected = list.Random();
            Assert.True(list.Contains(selected));
        }
    }
    
    [Fact]
    public void RandomElementIsNullForEmptyList() {
        NHamcrest.XUnit.Assert.That(new List<string>().Random(), Is.Null());
    }
    
    [Fact]
    public void RandomShouldBeRandom() {
        var list = new[] { "A", "B", "C", "D" };
        const int count = 10000000;
        var resultCount = new Dictionary<string, int>();
        for (int i = 0; i < count; i++) {
            var selected = list.Random();
            var currentCount = resultCount.ContainsKey(selected) ? resultCount[selected] : 0;
            resultCount[selected] = currentCount + 1;
        }

        var average = (int)Math.Round(count / (double)list.Length);
        var underThreshold = resultCount.Count(x => x.Value < 0.999 * average);
        var overThreshold = resultCount.Count(x => x.Value > 1.001 * average);
        NHamcrest.XUnit.Assert.That(resultCount.Count, Is.EqualTo(list.Count()));
        NHamcrest.XUnit.Assert.That(underThreshold, Is.EqualTo(0));
        NHamcrest.XUnit.Assert.That(overThreshold, Is.EqualTo(0));
    }
}
