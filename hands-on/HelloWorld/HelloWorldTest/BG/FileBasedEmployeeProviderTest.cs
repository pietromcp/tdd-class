using System;
using System.IO;
using System.Linq;
using HelloWorld.BG;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace HelloWorldTest.BG;

public class FileBasedEmployeeProviderTest {
    [Fact]
    public void WhenFileDoesNotExistThenReturnsEmptyList() {
        EmployeeProvider provider = new FileBasedEmployeeProvider("this-file-does-not-exist.txt");
        var employees = provider.GetAll();
        Assert.That(employees.Count(), Is.EqualTo(0));
    }
    
    [Fact]
    public void WhenFileIsEmptyThenReturnsEmptyList() {
        var tempFile = Path.GetTempFileName();
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employees = provider.GetAll();
        Assert.That(employees.Count(), Is.EqualTo(0));
    }

    [Fact]
    public void CanReadAllEmployees() {
        var tempFile = Path.GetTempFileName();
        File.WriteAllText(tempFile, @"last_name, first_name, date_of_birth, email
 Doe, John, 1982/10/08, john.doe@foobar.com
 Ann, Mary, 1975/09/11, mary.ann@foobar.com
 Rossi, Paola, 1925/01/17, paola.rossi@foobar.com
");
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employees = provider.GetAll();
        Assert.That(employees.Count(), Is.EqualTo(3));
    }
    
    [Fact]
    public void CanReadFirstName() {
        var tempFile = Path.GetTempFileName();
        File.WriteAllText(tempFile, @"last_name, first_name, date_of_birth, email
 Doe, John, 1982/10/08, john.doe@foobar.com
");
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employee = provider.GetAll().First();
        Assert.That(employee.FirstName, Is.EqualTo("John"));
    }
    
    [Fact]
    public void CanReadLastName() {
        var tempFile = Path.GetTempFileName();
        File.WriteAllText(tempFile, @"last_name, first_name, date_of_birth, email
 Doe, John, 1982/10/08, john.doe@foobar.com
");
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employee = provider.GetAll().First();
        Assert.That(employee.LastName, Is.EqualTo("Doe"));
    }
    
    [Fact]
    public void CanReadEmail() {
        var tempFile = Path.GetTempFileName();
        File.WriteAllText(tempFile, @"last_name, first_name, date_of_birth, email
 Doe, John, 1982/10/08, john.doe@foobar.com
");
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employee = provider.GetAll().First();
        Assert.That(employee.Email, Is.EqualTo("john.doe@foobar.com"));
    }
    
    [Fact]
    public void CanReadDateOfBirth() {
        var tempFile = Path.GetTempFileName();
        File.WriteAllText(tempFile, @"last_name, first_name, date_of_birth, email
 Doe, John, 1982/10/08, john.doe@foobar.com
");
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employee = provider.GetAll().First();
        Assert.That(employee.DateOfBirth, Is.EqualTo(new DateOnly(1982, 10, 8)));
    }
}
