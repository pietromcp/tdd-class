using System;
using System.Collections.Generic;
using System.Linq;
using HelloWorld.BG;
using Moq;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace HelloWorldTest.BG;

public class BirthdayGreetingsServiceTest {
    [Fact]
    public void CanNotifyGreetingsToOneEmployee() {
        var pietro = new Employee("Pietro", "Martinelli", new DateOnly(1978, 3, 19), "pietro@codiceplastico.com");
        var madda = new Employee("Maddalena", "Germinario", new DateOnly(1984, 11, 8), "maddalena@codiceplastico.com");
        EmployeeProvider repo = new FixedEmployeeProvider {
            Employees = new[] { pietro, madda }
        };
        GreetingsNotifier notifier = new InMemoryNotifier();
        var service = new BirthdayGreetingsService(repo, notifier);
        var now = new DateOnly(2023, 3, 19);
        service.Run(now);
        var notified = ((InMemoryNotifier)notifier).NotifiedEmployees;
        Assert.That(notified.Count(), Is.EqualTo(1));
        Assert.That(notified.First(), Is.EqualTo(pietro));
    }
    
    [Fact]
    public void CanNotifyGreetingsToOneEmployee_TheMoqVersion() {
        var pietro = new Employee("Pietro", "Martinelli", new DateOnly(1978, 3, 19), "pietro@codiceplastico.com");
        var madda = new Employee("Maddalena", "Germinario", new DateOnly(1984, 11, 8), "maddalena@codiceplastico.com");
        var repo = new Mock<EmployeeProvider>();
        repo.Setup(x => x.GetAll()).Returns(new[] { pietro, madda });
        var notifier = new Mock<GreetingsNotifier>();
        var service = new BirthdayGreetingsService(repo.Object, notifier.Object);
        var now = new DateOnly(2023, 3, 19);
        service.Run(now);
        
        notifier.Verify(x => x.NotifyBirthdayGreetings(pietro), Times.Once);
    }
}

public class InMemoryNotifier : GreetingsNotifier {
    private readonly IList<Employee> employees = new List<Employee>();
    
    public void NotifyBirthdayGreetings(Employee employee) {
        employees.Add(employee);
    }

    public IList<Employee> NotifiedEmployees => employees;
}

public class FixedEmployeeProvider : EmployeeProvider {
    public IEnumerable<Employee> GetAll() {
        return Employees;
    }

    public Employee[] Employees { get; init; }
}
