using System;
using System.IO;
using System.Text;
using HelloWorld.BG;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace HelloWorldTest.BG; 

public class ConsoleNotifierTest {
    [Fact]
    public void ShouldNotifyGreetingsInConsole() {
        GreetingsNotifier notifier = new ConsoleGreetingsNotifier();
        var originalSysOut = Console.Out;
        var outputStream = new MemoryStream();
        var writer = new StreamWriter(outputStream);
        Console.SetOut(writer);
        notifier.NotifyBirthdayGreetings(new Employee("Pietro", "Martinelli", new DateOnly(1978, 3, 19), "pietro@codiceplastico.com"));
        writer.Flush();
        var result = Encoding.UTF8.GetString(outputStream.ToArray());
        Assert.That(result.Trim(), Is.EqualTo("HappyBirthday Pietro! (pietro@codiceplastico.com)"));
        Console.SetOut(originalSysOut);
    }
}