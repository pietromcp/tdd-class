using HelloWorld;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace HelloWorldTest; 

public class HelloStrictTest {
    [Fact]
    public void ShouldSayHelloWorld() {
        var hello = new HelloStrict();
        var result = hello.SayHello();
        Assert.That(result, Is.EqualTo("Hello, World!"));
    }

    [Theory]
    [InlineData("pietrom", "Hello, pietrom!")]
    [InlineData("martinellip", "Hello, martinellip!")]
    public void CanProvideCustomTarget(string to, string expected) {
        var hello = new HelloStrict();
        var result = hello.SayHello(to);
        Assert.That(result, Is.EqualTo(expected));
    }
    
    [Theory]
    [InlineData("")]
    [InlineData(null)]
    public void SpecialCases(string to) {
        var hello = new HelloStrict();
        var result = hello.SayHello(to);
        Assert.That(result, Is.EqualTo("Hello, Anonymous!"));
    }
}