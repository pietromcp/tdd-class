using HelloWorld;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace HelloWorldTest; 

public class StringCalculatorTest {
    private readonly StringCalculator calculator;

    public StringCalculatorTest() {
        calculator = new StringCalculator();
    }

    [Fact]
    public void WhenInputIsEmptyThenOutputIsZero() {
        int result = calculator.Calculate("");
        Assert.That(result, Is.EqualTo(0));
    }

    [Fact]
    public void WhenInputContainsOneNumberThenOutputIsTheNumber() {
        var n = 19;
        int result = calculator.Calculate($"{n}");
        Assert.That(result, Is.EqualTo(n));
    }
    
    [Fact]
    public void WhenInputContainsManyNumbersThenOutputIsTheSum() {
        int result = calculator.Calculate("19,17,11,22");
        Assert.That(result, Is.EqualTo(69));
    }
    
    [Fact]
    public void WhenInputContainsManyNumbersThenOutputIsTheSum_MoreThanOneLine() {
        int result = calculator.Calculate("19,17,11,22\n1,2,3,4\n10,20,30,40");
        Assert.That(result, Is.EqualTo(179));
    }

    [Fact]
    public void CanSpecifyCustomSeparatorInTheFirstLine() {
        int result = calculator.Calculate("::|\n19|17|11|22\n1|2|3|4\n10|20|30|40");
        Assert.That(result, Is.EqualTo(179));
    }
}