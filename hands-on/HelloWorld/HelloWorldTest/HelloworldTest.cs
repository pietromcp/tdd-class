using HelloWorld;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace HelloWorldTest;

public class HelloWorldTest {
    [Fact]
    public void ShouldSayHelloWorld() {
        var hello = new Hello();
        var result = hello.SayHello();
        Assert.That(result, Is.EqualTo("Hello, World!"));
    }

    [Fact]
    public void ShouldSayHelloAnonymous_Null() {
        var hello = new Hello();
        var result = hello.SayHello(null);
        Assert.That(result, Is.EqualTo("Hello, Anonymous!"));
    }

    [Fact]
    public void ShouldSayHelloAnonymous_Empty() {
        var hello = new Hello();
        var result = hello.SayHello("");
        Assert.That(result, Is.EqualTo("Hello, Anonymous!"));
    }
}
