﻿namespace BirthdayGreeter;

public static class Program {
    public static void Main(string[] args) {
        var lines = File.ReadAllLines("employees.txt").Skip(1);
        foreach (var line in lines) {
            var fields = line.Split(',').Select(x => x.Trim()).ToArray();
            var dateAsText = fields[2];
            var date = DateOnly.ParseExact(dateAsText, "yyyy/MM/dd");
            var now = DateTime.Now;
            if (date.Day == now.Day && date.Month == now.Month) {
                Console.WriteLine($"HappyBirthday {fields[1]}! ({fields[3]})");   
            }
        }
    }
}