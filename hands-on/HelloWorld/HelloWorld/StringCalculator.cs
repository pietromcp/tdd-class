namespace HelloWorld;

public class StringCalculator {
    public int Calculate(string input) {
        if (input == "") {
            return 0;
        }

        var lines = input.Split('\n');
        var separator = lines[0].StartsWith("::") 
            ? lines[0].Substring(2, 1)[0] 
            : ',';
        var dataLines = lines[0]
            .StartsWith("::")
            ? lines.Skip(1).ToArray()
            : lines;

        return dataLines
            .SelectMany(x => x.Split(separator))
            .Select(int.Parse)
            .Sum();
    }
}
