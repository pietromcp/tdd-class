namespace HelloWorld; 

public class Hello {
    public string SayHello(string to) {
        var target = string.IsNullOrEmpty(to) ? "Anonymous" : to;
        return $"Hello, {target}!";
    }
    
    public string SayHello() {
        return "Hello, World!";
    }
}
