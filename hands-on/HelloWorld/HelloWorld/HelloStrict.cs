namespace HelloWorld;

public class HelloStrict {
    public string SayHello() {
        return SayHello("World");
    }

    public string SayHello(string to) {
        var target = string.IsNullOrEmpty(to) ? "Anonymous" : to;
        return $"Hello, {target}!";
    }
}