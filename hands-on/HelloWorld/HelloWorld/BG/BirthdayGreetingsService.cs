namespace HelloWorld.BG;

public class BirthdayGreetingsService {
    private readonly EmployeeProvider repo;
    private readonly GreetingsNotifier notifier;

    public BirthdayGreetingsService(EmployeeProvider repo, GreetingsNotifier notifier) {
        this.repo = repo;
        this.notifier = notifier;
    }

    public void Run(DateOnly now) {
        var toBeNotified = repo.GetAll()
            .Where(x => x.DateOfBirth.Day == now.Day && x.DateOfBirth.Month == now.Month);
        foreach (var employee in toBeNotified) {
            notifier.NotifyBirthdayGreetings(employee);
        }
    }
}