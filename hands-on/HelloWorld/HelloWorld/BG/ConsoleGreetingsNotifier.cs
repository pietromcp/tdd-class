namespace HelloWorld.BG;

public class ConsoleGreetingsNotifier : GreetingsNotifier {
    public void NotifyBirthdayGreetings(Employee employee) {
        Console.WriteLine($"HappyBirthday {employee.FirstName}! ({employee.Email})");
    }
}
