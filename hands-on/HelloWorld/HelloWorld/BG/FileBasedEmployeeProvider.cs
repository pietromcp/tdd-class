namespace HelloWorld.BG;

public class FileBasedEmployeeProvider : EmployeeProvider {
    private readonly string fileName;

    public FileBasedEmployeeProvider(string fileName) {
        this.fileName = fileName;
    }

    public IEnumerable<Employee> GetAll() {
        if (!File.Exists(fileName)) {
            yield break;
        }
        var allLines = File.ReadAllLines(fileName);
        var lines = allLines.Skip(1);
        foreach (var line in lines) {
            var fields = line.Split(',').Select(x => x.Trim()).ToArray();
            var dateAsText = fields[2];
            var date = DateOnly.ParseExact(dateAsText, "yyyy/MM/dd");
            yield return new Employee(fields[1], fields[0], date, fields[3]);
        }
    }
}
