namespace HelloWorld.BG; 

public interface EmployeeProvider {
    IEnumerable<Employee> GetAll();
}

public record Employee(string FirstName, string LastName, DateOnly DateOfBirth, string Email);