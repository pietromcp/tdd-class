namespace HelloWorld.BG;

public interface GreetingsNotifier {
    void NotifyBirthdayGreetings(Employee employee);
}
