namespace HelloWorld; 

public static class ListExtensions {
    private static readonly Random Rnd = new Random();
    
    public static T Random<T>(this IList<T> self) {
        if (self == null || !self.Any()) {
            return default(T);
        }
        return self[Rnd.Next(0, self.Count)];
    }
}
