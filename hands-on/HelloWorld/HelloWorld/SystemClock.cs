namespace HelloWorld;

public class SystemClock {
    public DateTimeOffset Now => DateTimeOffset.Now;
}