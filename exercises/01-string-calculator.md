# Test cases
- calculate("") → 0
- calculate(null) → 0
- calculate("19") → 19
- calculate("19,11,17") → 47
- calculate("19,11,17\n6,21\n22") → 96
- calculate("::|\n19|11|17\n6|21\n22") → 96