# Test cases
- sayHello() → "Hello, World!"
- sayHello("pietrom") → "Hello, pietrom!"
- sayHello(null) → "Hello, Anonymous!"
- sayHello("") → "Hello, Anonymous!"
